// Lottery Odds Calculator
// By: Jesse C. Fisher
// JesseCFisher@gmail.com

// Global Variables
 var Powerball_message = "";
 var Bonusball_message = "";
 var Match_message = "";
 var Number_Of_Tickets;
 var Tickets_Played_Output = 0;
 var Number_Of_Selections;
 var Number_Of_Balls;
 var Number_Of_PowerBalls_To_Match;
 var Total_Number_Of_PowerBalls;
 var Number_Of_Bonus_Balls;


function RunCalculator() {

  with (document.LotteryOddsInput)

 {

// Clear any previous results
 	OddsLevels.innerHTML = "";
  Tickets_Played_Output = 0;

 Number_Of_Selections = parseInt(Number_Of_Selections_Input.value);
 Number_Of_Balls = parseInt(Total_Number_Of_Balls_Input.value);
 Number_Of_PowerBalls_To_Match = parseInt(Powerballs.value);
 Total_Number_Of_PowerBalls = parseInt(Total_Powerballs.value);
 Number_Of_Bonus_Balls = parseInt(Number_Of_Bonus_Balls_Input.value);
 Number_Of_Tickets = parseInt(Number_Of_Tickets_Played_Input.value);
 

 // Validation

 if (Number_Of_Balls <= Number_Of_Selections) { alert('Number of Main Balls must be greater than number of balls drawn.'); return;}
 if (Number_Of_Balls <= 0) { alert('Number of Main Balls must be greater than 0.'); return;}
 if (Number_Of_Selections <= 0) { alert('Number of Balls Drawn must be greater than 0.'); return;}
 if (Number_Of_Bonus_Balls <= 0) { alert('Number of Bonus Balls must be greater than 0.'); return;}
 if (Total_Number_Of_PowerBalls <= 0) { alert('Number of Powerballs must be greater than 0.'); return;}
 if (Number_Of_PowerBalls_To_Match <= 0) { alert('Number of Powerballs drawn must be greater than 0.'); return;}
 if (Number_Of_Tickets <= 0) { alert('If you don\'t play you can\'t win. Number of tickets played must be greater than 0.'); return;}

 if (Number_Of_Balls < Number_Of_Selections + Number_Of_Bonus_Balls) { alert('Total number of Main Balls must be greater than number of Balls Drawn plus Bonus Balls.'); return;}

 if (Total_Number_Of_PowerBalls <= Number_Of_PowerBalls_To_Match) { alert('Number of Powerballs must be greater than number of Powerballs drawn.'); return;}


 // If Powerball is Unchecked set values to 0
 if (powerball.className == 'hidden') { Number_Of_PowerBalls_To_Match = 0; Total_Number_Of_PowerBalls = 0; }

 var odds = 0;
 var power_odds = 0;
 var combined_odds = 0;
  
  //Has no Power or Bonus Balls
  if ( (powerball.className == 'hidden') && (bonusball.className == 'hidden') ) {

     	for (var i = Number_Of_Selections; i >= 1; i--) {
    	
    	odds = getodds(Number_Of_Balls, Number_Of_Selections, i);

      printresults(odds, i);

     	};
  }


  //Has Bonus Balls
  if ( (powerball.className == 'hidden') && (bonusball.className == 'show') ) {

      for (var i = Number_Of_Selections; i > 0; i--) {
      
          for (var k = Number_Of_Bonus_Balls; k >= 0; k--) {
            
            // This if statment ensures that if a bonus ball can not be included in a calculation
            // that calculation is not printed multiple times.
            // For instance, you select 6 balls and there are 3 bonus balls
            // You can not match 6 balls + 3 bonus balls
            // Nor can you match 6 balls + 2 bonus balls
            // etc... without this validation the values for those would be calculated and displayed
            // The continue statement will break the current loop and continue with the next value.
            // http://www.w3schools.com/js/js_break.asp

            if (k + i > Number_Of_Selections) { continue;}

            odds = getOddsWithBonusballs(i, k);
            
            printresults(odds, i);

          }; // end Bonus Ball iteration 
      }; // end main lottery iteration
  }
	
  //Has Powerball
  if ( (powerball.className == 'show') && (bonusball.className == 'hidden') ) {

      // iterate through main lottery matches
      for (var i = Number_Of_Selections; i > 0; i--) {
        
          // iterate through Powerball matches
        for (var j = Number_Of_PowerBalls_To_Match; j >= 0; j--) {

                        odds = getodds(Number_Of_Balls, Number_Of_Selections, i);                                     
                        odds = getOddsWithPowerballs(odds, j);
                        odds = Math.round(odds);
                
                printresults(odds, i);

            }; //end of Powerball iteration

      };
  }

  //Has both Power and Bonus Balls
  if ( (powerball.className == 'show') && (bonusball.className == 'show') ) {

      // iterate odds of MAIN draw matching All balls down to 1 ball in the main draw
      for (var i = Number_Of_Selections; i >= 1; i--) {

            // iterate through Bonus Balls
            for (var k = Number_Of_Bonus_Balls; k >= 0; k--) {

                if (k + i > Number_Of_Selections) { continue;}          
                odds = getOddsWithBonusballs(i, k);
                  
                          // Iterate through Powerballs
                                          for (var j = Number_Of_PowerBalls_To_Match; j >= 0; j--) {
                                                    
                                              power_odds = getOddsWithPowerballs(odds, j);
                                              power_odds = Math.round(power_odds);
                                              printresults(power_odds, i);
                                          
                                          }; //end of Powerball iteration
            }; //end of Bonus ball iteration
      }; //end of main lotto iteration
    }
  }
}

function checktoggle(textboxid) {

if (textboxid.className == 'hidden') {textboxid.className = 'show';}
else {textboxid.className = 'hidden';}

}﻿

function getodds(Number_Of_Balls, Number_Of_Selections, i) {
  odds = 1 / ( ( combine(Number_Of_Selections, i ) * (combine(Number_Of_Balls - Number_Of_Selections, (Number_Of_Selections - i) ) ) ) / (combine(Number_Of_Balls, Number_Of_Selections)) );
  return(odds);
}

function combine(total, pick) {

  var i;
  var result = 1;

  for (i = total; i >= total - pick + 1; i-- ) {
    result *= i;
  }

  for (i = pick; i >= 1; i-- ) {
    result /= i;
  }

  return (result);
}


function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function combopage() {
    with (document.LotteryOddsInput)
    {
      ticketsplayedcontainer.className = "notdisplayed";
      
      pageheader.innerHTML = "<h2>Combination Calculator</h2>";
      calculatebutton.innerHTML = "<INPUT TYPE=button NAME=Button VALUE='Calculate The Combinations' onClick='RunCalculator()'>";
    }
}

function oddspage() {
    with (document.LotteryOddsInput)
    {
      ticketsplayedcontainer.className = "show";
      
      OddsLevels.innerHTML = "";
      pageheader.innerHTML = "<h2>Odds Calculator</h2>";


      calculatebutton.innerHTML = "<INPUT TYPE=button NAME=Button VALUE='Calculate The Odds' onClick='RunCalculator()'>";
    }
}


function getOddsWithBonusballs( i, k) {

  if (k > 1) {
    Bonusball_message = " + " + k + " Bonusballs ";

              // formula for matching bonus balls
              
              combined_odds = combine(Number_Of_Selections, i) * combine(Number_Of_Balls - Number_Of_Selections, Number_Of_Selections - i);
              
              for (var j = k; j >= 1; j--) {
                combined_odds = combined_odds * (j/(Number_Of_Balls - Number_Of_Selections - k + j));
              };
              
              combined_odds = 1 / ( combined_odds / combine(Number_Of_Balls,Number_Of_Selections));
            }

  if (k == 1) {
    Bonusball_message = " + 1 Bonusball ";
    combined_odds =  ( combine(Number_Of_Balls, Number_Of_Selections) / (combine(Number_Of_Selections, i) * ( combine(Number_Of_Balls - Number_Of_Selections, Number_Of_Selections - i) - ( combine(Number_Of_Balls - Number_Of_Selections - Number_Of_Bonus_Balls, Number_Of_Selections - i)) )));
    combined_odds = Math.round(combined_odds);
  }

  if ( (k == 0) || (Number_Of_Selections < i + k) ) {
    Bonusball_message = "";                
             
                combined_odds = (combine(Number_Of_Balls, Number_Of_Selections)/ (combine(Number_Of_Selections, i) * combine(Number_Of_Balls - Number_Of_Selections - Number_Of_Bonus_Balls, Number_Of_Selections - i) ));
              }
              return combined_odds;
}

function getOddsWithPowerballs(odds, j) {


// iteration to Calculate odds of matching Powerballs
              // If matching more than 1 Powerball, use the multi-powerball odds calculation then multiply by main lottery odds
              if (j > 0) { 
                power_odds = getodds(Total_Number_Of_PowerBalls, Number_Of_PowerBalls_To_Match, j);
                combined_odds = odds * power_odds;
                Powerball_message = " + " + j + " Powerballs ";             
              };
              
              // If matching 1 Powerball, multiply total number of powerballs by main lottery odds
              if (j == 1) { 
       
                  Powerball_message = " + 1 Powerball ";
              };

              // If matching NO Powerballs 
              if (j == 0) { 
                power_odds = getodds(Total_Number_Of_PowerBalls, Number_Of_PowerBalls_To_Match, j)
                combined_odds = odds * power_odds;
                Powerball_message = "";
              };
          return combined_odds;

}


    function isNumberKey(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

      return true;
    }

function printresults(odds, i) {
  
  odds = Math.round(odds);

  if ( (Tickets_Played_Output < 1) && (numberoftickets.className == 'show') ) { OddsLevels.innerHTML += "<div class='ticketsplayed'>" + "Playing " + Number_Of_Tickets + " tickets, your chances of winning the jackpot are 1 in " + numberWithCommas(Math.round(odds / Number_Of_Tickets)) + "</div>" + "<br />"; Tickets_Played_Output++; }

  // These three if statements create the output for the Combinations Tab calculation
  if ( (ticketsplayedcontainer.className == "notdisplayed") && (powerball.className == 'show')  ) { Powerball_message = " from the main numbers, and " + numberWithCommas(odds) + " different combinations when you add in the Powerballs.";}
  if ( (ticketsplayedcontainer.className == "notdisplayed") && (powerball.className == 'hidden') ){ Powerball_message = ".";}
  if (  ticketsplayedcontainer.className == "notdisplayed") { OddsLevels.innerHTML = "<div class='numberofcombinations'>" + "There are " + numberWithCommas( Math.round( (odds / getodds(Total_Number_Of_PowerBalls, Number_Of_PowerBalls_To_Match, Number_Of_PowerBalls_To_Match))) ) + " different combinations" + Powerball_message + "</div>"; throw "stop execution";}

  else { OddsLevels.innerHTML += "<div class='numberofmatches'>" + "Match " + i + Powerball_message + Bonusball_message + "</div>" + "<div class='oddlevelresult'>" + 1 + " in " + numberWithCommas(odds) + "</div>" + "<br />";}
}